package addr;

public class Decoup {
	
	private int adr1=192, adr2=168, adr3=1,adr4=64,mask=25;
	private int nbrPC;
	public int nbrSousReseau;
	/**
	 * @return the nbrPC
	 */
	public int getNbrPC() {
		return nbrPC;
	}
	/**
	 * @param nbrPC the nbrPC to set
	 */
	public void setNbrPC(int nbrPC) {
		this.nbrPC = nbrPC;
	}
	
	/**
	 * @return the adr1
	 */
	public int getAdr1() {
		return adr1;
	}
	/**
	 * @param adr1 the adr1 to set
	 */
	public void setAdr1(int adr1) {
		this.adr1 = adr1;
	}
	/**
	 * @return the adr2
	 */
	public int getAdr2() {
		return adr2;
	}
	/**
	 * @param adr2 the adr2 to set
	 */
	public void setAdr2(int adr2) {
		this.adr2 = adr2;
	}
	/**
	 * @return the adr3
	 */
	public int getAdr3() {
		return adr3;
	}
	/**
	 * @param adr3 the adr3 to set
	 */
	public void setAdr3(int adr3) {
		this.adr3 = adr3;
	}
	/**
	 * @return the adr4
	 */
	public int getAdr4() {
		return adr4;
	}
	/**
	 * @param adr4 the adr4 to set
	 */
	public void setAdr4(int adr4) {
		this.adr4 = adr4;
	}
	/**
	 * @return the mask
	 */
	public int getMask() {
		return mask;
	}
	/**
	 * @param mask the mask to set
	 */
	public void setMask(int mask) {
		this.mask = mask;
	}
	public int findN(){
		//setNbrPC(75);
		int nbrPC = getNbrPC();
		int n;
		for (n = 0; nbrPC >= Math.pow(2, n)-2; n++) {
			//n=n+1;
		}
		return n;
		
	}
	
	public int nextMask(){
		int n = findN();
		int masquesuivant = 32-n;
		return masquesuivant;
	}
	public Decoup(int n1,int n2,int n3,int n4,int mask,int nbrPC){
		adr1 = n1;
		adr2 = n2;
		adr3 = n3;
		adr4 = n4;
		this.mask = mask;
		this.nbrPC = nbrPC;
		Decouper();
	}
	public void Decouper(){
		int[] adress = {getAdr1(),getAdr2(),getAdr3(),getAdr4(),getMask()};
		int n;
		
		System.out.println(adress[0]+"."+adress[1]+"."+adress[2]+"."+adress[3]+"/"+adress[4]);
		
		n = findN();
		
		System.out.println("Nombre de PC: "+nbrPC+"\nn = " + n);
		System.out.println("2^n = " +(int) Math.pow(2, n));;
		
		
		int k = nextMask()-adress[4];
		nbrSousReseau = (int)Math.pow(2, k);
		
		System.out.println("Sous-reseau =" +nbrSousReseau);
		System.out.println("Masque suivant: "+nextMask());
		
		setMask(nextMask());
		setAdr3(adress[2]);
		setAdr4(adress[3]);
		
		System.out.println(adress[0]+"."+adress[1]+"."+adress[2]+"."+adress[3]+"/"+nextMask());
		

	}
	public void nextAdress(){
		int n=findN();
		setAdr4(getAdr4() +(int) Math.pow(2, n));

		if (nbrSousReseau == 1) {
			setAdr3(getAdr3()+1);
			setAdr4(0);
			setMask(24);
		}

		System.out.println(getAdr1()+"."+getAdr2()+"."+getAdr3()+"."+getAdr4()+"/"+getMask());
	}
	
}