package addr;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;






import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;





import org.omg.CORBA.PUBLIC_MEMBER;

import java.awt.Color;

class Adresse{
	
private int n1,n2,n3,n4,masque,nbre;
private int n;
//public int masquesuivant;

	public Adresse(int nv1,int nv2,int nv3,int nv4,int vmasque,int vnbre){
		this.n1 = nv1;
		this.n2 =nv2;
		this.n3 =nv3;
		this.n4 =nv4;
		this.masque = vmasque;
		this.nbre = vnbre;
		
	}
	
	public void afficher() {
		System.out.println(n1+"."+n2+"."+n3+"."+n4+"/"+masque+" "+"Nombre de pc:"+nbre);
	}
	
	public int trouverN(){
		for (n = 0; nbre >= Math.pow(2, n)-2; n++) {
			//n=n+1;
		}
		System.out.println("Pour "+nbre+" PC, n = " + n);
		
		//Essai.vnom.setValue(n);
		
		System.out.println("2^n = " +(int) Math.pow(2, n));;
		
		return n;
	}
	
	public void decoupe() {
		
		trouverN();
		
		int masquesuivant = 32-n;
		int k = masquesuivant-masque;
		
		
		int nbrsousreseau = (int)Math.pow(2, k);
		
		System.out.println("Nombre d'adresse sous reseau =" +nbrsousreseau);
		System.out.println("Masque suivant = /" +masquesuivant);
		
		System.out.println("Adresse 1 (adresse reseau): "+n1+"."+n2+"."+n3+"."+n4+"/"+masquesuivant);
		
		int n41 = n4 +(int) Math.pow(2, n);
		
		if (nbrsousreseau == 1) {
			n3=n3+1;
			n41 = 0;
			masquesuivant = 24;
		}
		
		System.out.println("Adresse 2:"+n1+"."+n2+"."+n3+"."+n41+"/"+masquesuivant);
	}
}


public class Main {

	private JFrame frame;
	public int n1,n2,n3,n4,masque,nbr;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Main window = new Main();
					
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Main() {
		initialize();
	}
	
	

	/**
	 * Initialize the contents of the frame.
	 */
	public void initialize() {
		
		
		
		frame = new JFrame();
		frame.getContentPane().setForeground(Color.BLACK);
		frame.getContentPane().setBackground(Color.GRAY);
		frame.setBounds(100, 100, 421, 333);
		//frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setResizable(false);
		frame.setUndecorated(true);
		
		JSpinner adr1 = new JSpinner();
		adr1.setModel(new SpinnerNumberModel(new Integer(192), null, null, new Integer(1)));
		adr1.setBounds(10, 41, 59, 20);
		frame.getContentPane().add(adr1);
		
		JSpinner adr2 = new JSpinner();
		adr2.setModel(new SpinnerNumberModel(new Integer(168), null, null, new Integer(1)));
		adr2.setBounds(90, 41, 59, 20);
		frame.getContentPane().add(adr2);
		
		JSpinner adr3 = new JSpinner();
		adr3.setModel(new SpinnerNumberModel(new Integer(1), null, null, new Integer(1)));
		adr3.setBounds(165, 41, 59, 20);
		frame.getContentPane().add(adr3);
		
		JSpinner adr4 = new JSpinner();
		adr4.setBounds(240, 41, 59, 20);
		frame.getContentPane().add(adr4);
		
		JSpinner mask = new JSpinner();
		mask.setModel(new SpinnerNumberModel(new Integer(24), null, null, new Integer(1)));
		mask.setBounds(337, 41, 48, 20);
		frame.getContentPane().add(mask);
		
		JSpinner nbrPC = new JSpinner();
		nbrPC.setBounds(100, 70, 72, 20);
		frame.getContentPane().add(nbrPC);
		
		JLabel label = new JLabel(".");
		label.setForeground(Color.WHITE);
		label.setBounds(79, 47, 13, 14);
		frame.getContentPane().add(label);
		
		JLabel label_1 = new JLabel(".");
		label_1.setForeground(Color.WHITE);
		label_1.setBounds(159, 47, 13, 14);
		frame.getContentPane().add(label_1);
		
		JLabel label_2 = new JLabel(".");
		label_2.setForeground(Color.WHITE);
		label_2.setBounds(234, 47, 13, 14);
		frame.getContentPane().add(label_2);
		
		JSpinner vnom = new JSpinner();
		vnom.setEnabled(false);
		vnom.setBounds(64, 160, 59, 20);
		frame.getContentPane().add(vnom);
		
		JButton btnDecouper = new JButton("Decouper");
		btnDecouper.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				n1 = (int) adr1.getValue();
				n2 = (int) adr2.getValue();
				n3 = (int) adr3.getValue();
				n4 = (int) adr4.getValue();
				masque = (int) mask.getValue();
				nbr = (int) nbrPC.getValue();
				
				//Scanner sc = new Scanner(System.in);
				Address a = new Address(n1, n2, n3, n4, masque, nbr);
				vnom.setValue(a.trouverN());
				a.decoupe();
				
			}
		});
		btnDecouper.setBounds(165, 125, 89, 23);
		frame.getContentPane().add(btnDecouper);
		
		JLabel lblSaisirLaddresseA = new JLabel("Saisir l'adresse a decouper");
		lblSaisirLaddresseA.setForeground(Color.WHITE);
		lblSaisirLaddresseA.setBounds(79, 11, 168, 25);
		frame.getContentPane().add(lblSaisirLaddresseA);
		
		JLabel label_6 = new JLabel("/");
		label_6.setForeground(Color.WHITE);
		label_6.setBounds(316, 44, 13, 14);
		frame.getContentPane().add(label_6);
		
		JLabel lblNombreDePc = new JLabel("Nombre de PC:");
		lblNombreDePc.setForeground(Color.WHITE);
		lblNombreDePc.setBounds(10, 72, 82, 14);
		frame.getContentPane().add(lblNombreDePc);
		
		JLabel lblN = new JLabel("n =");
		lblN.setForeground(Color.WHITE);
		lblN.setBounds(23, 163, 31, 14);
		frame.getContentPane().add(lblN);
		
		JButton btnSuivant = new JButton("Suivant");
		btnSuivant.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
			}
		});
		btnSuivant.setBounds(296, 125, 89, 23);
		frame.getContentPane().add(btnSuivant);
		
		JLabel lblAdresse = new JLabel("Adresse 1:");
		lblAdresse.setForeground(Color.WHITE);
		lblAdresse.setBounds(10, 195, 59, 14);
		frame.getContentPane().add(lblAdresse);
		
		JSpinner adr1_1 = new JSpinner();
		adr1_1.setBounds(23, 220, 59, 20);
		frame.getContentPane().add(adr1_1);
		
		JSpinner adr2_1 = new JSpinner();
		adr2_1.setBounds(103, 220, 59, 20);
		frame.getContentPane().add(adr2_1);
		
		JSpinner adr3_1 = new JSpinner();
		adr3_1.setBounds(178, 220, 59, 20);
		frame.getContentPane().add(adr3_1);
		
		JSpinner adr4_1 = new JSpinner();
		adr4_1.setBounds(253, 220, 59, 20);
		frame.getContentPane().add(adr4_1);
		
		JLabel label_6_1 = new JLabel("/");
		label_6_1.setForeground(Color.WHITE);
		label_6_1.setBounds(329, 223, 13, 14);
		frame.getContentPane().add(label_6_1);
		
		JSpinner mask_1 = new JSpinner();
		mask_1.setBounds(350, 220, 48, 20);
		frame.getContentPane().add(mask_1);
		
		JLabel label_2_1 = new JLabel(".");
		label_2_1.setForeground(Color.WHITE);
		label_2_1.setBounds(247, 226, 13, 14);
		frame.getContentPane().add(label_2_1);
		
		JLabel label_1_1 = new JLabel(".");
		label_1_1.setForeground(Color.WHITE);
		label_1_1.setBounds(172, 226, 13, 14);
		frame.getContentPane().add(label_1_1);
		
		JLabel label_3 = new JLabel(".");
		label_3.setForeground(Color.WHITE);
		label_3.setBounds(92, 226, 13, 14);
		frame.getContentPane().add(label_3);
		
		JSpinner adr1_2 = new JSpinner();
		adr1_2.setBounds(23, 273, 59, 20);
		frame.getContentPane().add(adr1_2);
		
		JSpinner adr2_2 = new JSpinner();
		adr2_2.setBounds(103, 273, 59, 20);
		frame.getContentPane().add(adr2_2);
		
		JSpinner adr3_2 = new JSpinner();
		adr3_2.setBounds(178, 273, 59, 20);
		frame.getContentPane().add(adr3_2);
		
		JSpinner adr4_2 = new JSpinner();
		adr4_2.setBounds(253, 273, 59, 20);
		frame.getContentPane().add(adr4_2);
		
		JLabel label_6_2 = new JLabel("/");
		label_6_2.setForeground(Color.WHITE);
		label_6_2.setBounds(329, 276, 13, 14);
		frame.getContentPane().add(label_6_2);
		
		JSpinner mask_2 = new JSpinner();
		mask_2.setBounds(350, 273, 48, 20);
		frame.getContentPane().add(mask_2);
		
		JLabel label_2_2 = new JLabel(".");
		label_2_2.setForeground(Color.WHITE);
		label_2_2.setBounds(247, 279, 13, 14);
		frame.getContentPane().add(label_2_2);
		
		JLabel label_1_2 = new JLabel(".");
		label_1_2.setForeground(Color.WHITE);
		label_1_2.setBounds(172, 279, 13, 14);
		frame.getContentPane().add(label_1_2);
		
		JLabel label_4 = new JLabel(".");
		label_4.setForeground(Color.WHITE);
		label_4.setBounds(92, 279, 13, 14);
		frame.getContentPane().add(label_4);
		
		JLabel lblAdresse_1 = new JLabel("Adresse 2 :");
		lblAdresse_1.setForeground(Color.WHITE);
		lblAdresse_1.setBounds(10, 251, 59, 14);
		frame.getContentPane().add(lblAdresse_1);
		
		JButton btnNewButton = new JButton("Termin\u00E9");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		btnNewButton.setBounds(316, 299, 89, 23);
		frame.getContentPane().add(btnNewButton);
		
	}
	
}
