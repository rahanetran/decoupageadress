package addr;

import javax.swing.JFrame;
import javax.swing.JSpinner;
import javax.swing.JButton;
import javax.swing.JLabel;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.SpinnerNumberModel;

public class FenDecoup {

	public JFrame frame;

	/**
	 * Launch the application.
	 */
//	public static void main(String[] args) {
//		EventQueue.invokeLater(new Runnable() {
//			public void run() {
//				try {
//					FenDecoup window = new FenDecoup();
//					window.frame.setVisible(true);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
//	}

	/**
	 * Create the application.
	 */
	public FenDecoup() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	public void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JSpinner adr1 = new JSpinner();
		adr1.setModel(new SpinnerNumberModel(new Integer(192), null, null, new Integer(1)));
		adr1.setBounds(22, 30, 45, 20);
		frame.getContentPane().add(adr1);
		
		JSpinner adr2 = new JSpinner();
		adr2.setModel(new SpinnerNumberModel(new Integer(168), null, null, new Integer(1)));
		adr2.setBounds(93, 30, 45, 20);
		frame.getContentPane().add(adr2);
		
		JSpinner adr3 = new JSpinner();
		adr3.setModel(new SpinnerNumberModel(new Integer(1), null, null, new Integer(1)));
		adr3.setBounds(166, 30, 45, 20);
		frame.getContentPane().add(adr3);
		
		JSpinner adr4 = new JSpinner();
		adr4.setBounds(241, 30, 45, 20);
		frame.getContentPane().add(adr4);
		
		JSpinner mask = new JSpinner();
		mask.setModel(new SpinnerNumberModel(new Integer(24), null, null, new Integer(1)));
		mask.setBounds(330, 30, 45, 20);
		frame.getContentPane().add(mask);
		
		JSpinner n = new JSpinner();
		n.setEnabled(false);
		n.setBounds(50, 106, 45, 20);
		frame.getContentPane().add(n);
		
		
		
		
		JLabel lblN = new JLabel("n =");
		lblN.setBounds(10, 109, 46, 14);
		frame.getContentPane().add(lblN);
		
		JSpinner adr1_1 = new JSpinner();
		adr1_1.setEnabled(false);
		adr1_1.setBounds(78, 137, 45, 20);
		frame.getContentPane().add(adr1_1);
		
		JSpinner adr1_2 = new JSpinner();
		adr1_2.setEnabled(false);
		adr1_2.setBounds(147, 137, 45, 20);
		frame.getContentPane().add(adr1_2);
		
		JSpinner adr1_3 = new JSpinner();
		adr1_3.setEnabled(false);
		adr1_3.setBounds(222, 137, 45, 20);
		frame.getContentPane().add(adr1_3);
		
		JSpinner adr1_4 = new JSpinner();
		adr1_4.setEnabled(false);
		adr1_4.setBounds(282, 137, 45, 20);
		frame.getContentPane().add(adr1_4);
		
		JSpinner mask1 = new JSpinner();
		mask1.setEnabled(false);
		mask1.setBounds(360, 137, 45, 20);
		frame.getContentPane().add(mask1);
		
		JSpinner nbrPC = new JSpinner();
		nbrPC.setBounds(103, 58, 45, 20);
		frame.getContentPane().add(nbrPC);
		
		JLabel lblNombreDePc = new JLabel("Nombre de PC :");
		lblNombreDePc.setBounds(10, 61, 113, 14);
		frame.getContentPane().add(lblNombreDePc);
		
		JSpinner adr2_1 = new JSpinner();
		adr2_1.setEnabled(false);
		adr2_1.setBounds(78, 187, 45, 20);
		frame.getContentPane().add(adr2_1);
		
		JSpinner adr2_2 = new JSpinner();
		adr2_2.setEnabled(false);
		adr2_2.setBounds(147, 187, 45, 20);
		frame.getContentPane().add(adr2_2);
		
		JSpinner adr2_3 = new JSpinner();
		adr2_3.setEnabled(false);
		adr2_3.setBounds(222, 187, 45, 20);
		frame.getContentPane().add(adr2_3);
		
		JSpinner adr2_4 = new JSpinner();
		adr2_4.setEnabled(false);
		adr2_4.setBounds(282, 187, 45, 20);
		frame.getContentPane().add(adr2_4);
		
		JSpinner mask2 = new JSpinner();
		mask2.setEnabled(false);
		mask2.setBounds(360, 187, 45, 20);
		frame.getContentPane().add(mask2);
		
		JButton btnDecouper = new JButton("Decouper");
		btnDecouper.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int n1 = (int) adr1.getValue();
				int n2 = (int) adr2.getValue();
				int n3 = (int) adr3.getValue();
				int n4 = (int) adr4.getValue();
				int masque = (int) mask.getValue();
				int vnbrPC =(int) nbrPC.getValue();
				Decoup dec = new Decoup(n1,n2,n3,n4,masque,vnbrPC);
				//dec.setNbrPC((int)nbrPC.getValue());
				System.out.println(dec.nbrSousReseau);
				n.setValue(dec.findN());
				adr1_1.setValue(dec.getAdr1());
				adr1_2.setValue(dec.getAdr2());
				adr1_3.setValue(dec.getAdr3());
				adr1_4.setValue(dec.getAdr4());
				mask1.setValue(dec.getMask());
				dec.nextAdress();
				adr2_1.setValue(dec.getAdr1());
				adr2_2.setValue(dec.getAdr2());
				adr2_3.setValue(dec.getAdr3());
				adr2_4.setValue(dec.getAdr4());
				mask2.setValue(dec.getMask());
				
			}
		});
		btnDecouper.setBounds(178, 82, 89, 23);
		frame.getContentPane().add(btnDecouper);
		
		JLabel lblAdrRseau = new JLabel("Adr R\u00E9seau: ");
		lblAdrRseau.setBounds(10, 140, 70, 14);
		frame.getContentPane().add(lblAdrRseau);
		
		JLabel lblAdrSuivant = new JLabel("Adr Suivant: ");
		lblAdrSuivant.setBounds(10, 190, 70, 14);
		frame.getContentPane().add(lblAdrSuivant);
	}
}
